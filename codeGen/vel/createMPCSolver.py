import numpy as np
from casadi import sin, cos, dot
import forcespro
import forcespro.nlp
from forcespro import MultistageProblem
import casadi
from urdfpy import URDF

import pandaReacher
from pandaReacher.resources.pandaRobot import PandaRobot

import os
import sys
from scipy.integrate import odeint

n = 7

def getParameters(n):
    pm = {}
    pm['wu'] = list(range(0, n))
    pm['w'] = list(range(n, 2 * n))
    pm['g'] = list(range(2 * n, 3 * n))
    dt = 0.03
    npar = 3 * n
    N = 10
    nx = n
    nu = n
    return pm, npar, nx, nu, dt

paramMap, npar, nx, nu, dt = getParameters(n)

# file names
solverName = 'mpcSolver_panda'
robot = PandaRobot()

# MPC parameter
N = 10
limitPos, limitVel, _ = robot.getLimits()
xu = limitPos[1, :]
xl = limitPos[0, :]
uu = limitVel[1, :]
ul = limitVel[0, :]


def diagSX(val, size):
    a = casadi.SX(size, size)
    for i in range(size):
        a[i, i] = val[i]
    return a

def eval_obj(z, p):
    x = z[0:n]
    u = z[nx:nx+nu]
    w = p[paramMap['w']]
    wu = p[paramMap['wu']]
    g = p[paramMap['g']]
    W = diagSX(w, n)
    Wu = diagSX(wu, n)
    err = x - g
    Jx = casadi.dot(err, casadi.mtimes(W, err))
    Ju = casadi.dot(u, casadi.mtimes(Wu, u))
    J = Jx + Ju
    return J

def continuous_dynamics(x, u):
    vel = u
    return vel


def main():
    model = forcespro.nlp.SymbolicModel(N)
    model.objective = eval_obj
    model.eq = lambda z: forcespro.nlp.integrate(
        continuous_dynamics,
        z[0:nx],
        z[nx:nx+nu],
        integrator=forcespro.nlp.integrators.RK4,
        stepsize=dt
    )
    model.E = np.concatenate([np.eye(nx), np.zeros((nx, nu))], axis=1)
    model.lb = np.concatenate((xl, ul))
    model.ub = np.concatenate((xu, uu))
    model.npar = npar
    model.nvar = nx + nu
    model.neq = nx
    model.xinitidx = range(0, nx)


    # Get the default solver options
    codeoptions = forcespro.CodeOptions(solverName)
    codeoptions.printlevel = 0
    codeoptions.optlevel = 3
    codeoptions.maxit = 300
    codeoptions.solver_timeout = -1
    codeoptions.nlp.TolStat = -1 # default 1e-5
    codeoptions.nlp.TolEq = -1 # default 1e-6
    codeoptions.nlp.TolIneq = -1 # default 1e-6
    #codeoptions.nlp.integrator.type = "ERK2"
    #codeoptions.nlp.integrator.Ts = 0.1
    #codeoptions.nlp.integrator.nodes = 5
    # Generate solver for previously initialized model
    solver = model.generate_solver(codeoptions)

if __name__ == "__main__":
    main()
