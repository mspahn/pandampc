#!/usr/bin/env python3
import time
import numpy as np

import forcespro.nlp

import sys
import os
codeGenPath = os.path.dirname(os.path.abspath(__file__)) + "/../codeGen/"
sys.path.append(codeGenPath)
from vel.createMPCSolver import getParameters, N, n

import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState


paramMap, npar, nx, nu, dt = getParameters(n)
wx = np.ones(n)
wu = np.ones(n)

mpcFileName = '../codeGen/vel/mpcSolver_panda'
mpcSolver = forcespro.nlp.Solver.from_directory(mpcFileName)

class MpcSolver(object):

    _nx = nx
    _nu = nu
    _H = N
    _npar = npar
    _wx = wx
    _wu = wu

    def __init__(self, goalPos):
        self._x0 = np.zeros(shape=(self._H, self._nx+self._nu))
        self._x0[-1] = -0.1
        self._params = np.zeros(shape=(self._npar * self._H), dtype=float)
        for i in range(self._H):
            self._params[
                [self._npar * i + val for val in paramMap["w"]]
            ] = self._wx
            self._params[
                [self._npar * i + val for val in paramMap["wu"]]
            ] = self._wu
        self.setGoal(goalPos)

    def setGoal(self, goalPos):
        for i in range(self._H):
            for j in range(n):
                self._params[self._npar * i + paramMap["g"][j]] = goalPos[j]

    def shiftHorizon(self, output, ob):
        nvar = self._nx + self._nu
        for key in output.keys():
            stage = int(key[-2:])
            self._x0[stage-1,:] = output[key]

    def solve(self, ob):
        #print("Observation : " , ob[0:2*n])
        xinit = ob[0:nx]
        action = np.zeros(nu)
        problem = {}
        problem["ToleranceStationarity"] = 1e-7
        problem["ToleranceEqualities"] = 1e-7
        problem["ToleranceInequalities"] = 1e-7
        #problem["SolverTimeout"] = 0.0001
        #problem["ToleranceComplementarity"] = 1e-5
        problem["xinit"] = xinit
        problem["x0"] = self._x0.flatten()
        problem["all_parameters"] = self._params
        output, exitflag, info = mpcSolver.solve(problem)
        action = output["x01"][-nu:]
        #print("Prediction: " , output["x02"][0:2*n])
        self.shiftHorizon(output, ob)
        return action, info


class RosMpcAgent(object):
    def __init__(self, solver):
        self._solver = solver
        rospy.init_node("rosmpcagent")
        self._rate = rospy.Rate(1/dt)
        self._pub = rospy.Publisher('/panda/panda_joint_controllers/command', Float64MultiArray, queue_size=10)
        self._sub = rospy.Subscriber('/panda/joint_states', JointState, self.state_cb)
        self._q = np.zeros(7)
        self._qdot = np.zeros(7)

    def state_cb(self, jointStates):
        self._q = jointStates.position[2:9]
        self._qdot = jointStates.velocity[2:9]

    def loop(self):
        while not rospy.is_shutdown():
            action, info = self._solver.solve(self._q)
            rospy.loginfo("solvertime %s" % info.solvetime)
            self.sendAction(action)
            self._rate.sleep()

    def publishZeroVelocity(self):
        self.sendAction(np.zeros(7))

    def sendAction(self, qdot):
        action_message = Float64MultiArray()
        action_message.data = qdot
        self._pub.publish(action_message)

def main():
    goal = 1.0 * np.array([0, 0, 0, -1.501, 0, 1.8675, 0])
    solver = MpcSolver(goal)
    rosNode = RosMpcAgent(solver)
    try:
        rosNode.loop()
    except rospy.ROSInterruptException:
        pass

if __name__ == "__main__":
    main()
